package com.example.currencyconverter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button cancelBTN,convertBTN;
    EditText inputEDT;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.streamWidgets();
    }
    protected void streamWidgets(){
        this.cancelBTN = (Button) findViewById(R.id.cancelBTN);
        this.convertBTN = (Button) findViewById(R.id.convertBTN);
        this.inputEDT = (EditText) findViewById(R.id.inputEDT);
    }
    public void cancelFunc(View view){
        this.inputEDT.setText(null);
    }
    public void convertFunc(View view){

        int dollar = Integer.parseInt(this.inputEDT.getText().toString());
        int eq = dollar * 160000;
        Toast.makeText(this,eq + "Rials",Toast.LENGTH_LONG).show();


    }
}
